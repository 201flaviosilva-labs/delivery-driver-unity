using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delivery : MonoBehaviour {
    private bool havePackage = false;
    private float destroyDelay = 0.1f;

    private SpriteRenderer _spriteRender;

    [SerializeField] private Color32 packageColor = new Color32(1, 1, 1, 1); // Green (changed in inspector)
    [SerializeField] private Color32 normalColor = new Color32(1, 1, 1, 1); // White (changed in inspector)

    private void Awake() {
        _spriteRender = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Package" && !havePackage) {
            Destroy(other.gameObject, destroyDelay);
            havePackage = true;
            _spriteRender.color = packageColor;
        } else if (other.tag == "Costumer" && havePackage) {
            havePackage = false;
            _spriteRender.color = normalColor;
        }
    }
}