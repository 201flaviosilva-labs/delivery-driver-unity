using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour {
    [SerializeField] private float steerSpeed;
    [SerializeField] private float movingSpeed;

    private float slowSpeed = 0.5f;
    private float normalSpeed;
    private float bostSpeed = 2f;

    private float steerAmout = 0.0f;
    private float movingAmout = 0.0f;

    private void Start() {
        normalSpeed = movingSpeed;
    }

    private void Update() {
        steerAmout = Input.GetAxis("Horizontal") * steerSpeed * Time.deltaTime;
        movingAmout = Input.GetAxis("Vertical") * movingSpeed * Time.deltaTime;
    }

    private void FixedUpdate() {
        float steerByMovingSpeed = movingAmout < 0 ? steerAmout : -steerAmout;
        transform.Rotate(0, 0, steerByMovingSpeed);
        transform.Translate(0, movingAmout, 0);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "boost") {
            movingSpeed = normalSpeed * bostSpeed;
        } else if (other.tag == "Slow") {
            movingSpeed = normalSpeed * slowSpeed;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "boost" || other.tag == "Slow") {
            movingSpeed = normalSpeed;
        }
    }
}