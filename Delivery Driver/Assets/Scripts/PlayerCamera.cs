using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {
    [SerializeField] private Transform carPlayer;

    private void LateUpdate() {
        transform.position = new Vector3(
            carPlayer.position.x,
            carPlayer.position.y,
            transform.position.z);
        transform.rotation = carPlayer.rotation;
    }
}